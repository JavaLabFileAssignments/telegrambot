package uz.bot;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.objects.*;
import org.telegram.telegrambots.meta.api.objects.polls.Poll;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButtonPollType;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class MyFirstBot extends TelegramLongPollingBot {

    private final SecureToken secureToken = new SecureToken();
    private final SendMessage sendMessage = new SendMessage();
    private Message message;
    private String text;
    @Override
    public String getBotUsername() {
        return "Muzammil_Tohir_bot";
    }

    @Override
    public String getBotToken() {
        return secureToken.getToken();
    }

    @Override
    public void onUpdateReceived(Update update) {
        if (update.hasMessage() && update.getMessage().hasText() ) {
            // Set variables
            String message_text = update.getMessage().getText();
            long chat_id = update.getMessage().getChatId();
            if (!message_text.equals("Tilni tanlang") && !message_text.equals("/start")) {
                SendMessage message = new SendMessage() // Create a message object object
                        .setChatId(chat_id)
                        .setText(message_text);
                try {
                    execute(message); // Sending our message object to user
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            }
        }
//        if (update.hasMessage() && update.getMessage().hasPhoto()) {
//            // Message contains photo
//            // Set variables
//            long chat_id = update.getMessage().getChatId();
//
//            // Array with photo objects with different sizes
//            // We will get the biggest photo from that array
//            List<PhotoSize> photos = update.getMessage().getPhoto();
//            // Know file_id
//            String f_id = photos.stream()
//                    .sorted(Comparator.comparing(PhotoSize::getFileSize).reversed())
//                    .findFirst()
//                    .orElse(null).getFileId();
//            // Know photo width
//            int f_width = photos.stream()
//                    .sorted(Comparator.comparing(PhotoSize::getFileSize).reversed())
//                    .findFirst()
//                    .orElse(null).getWidth();
//            // Know photo height
//            int f_height = photos.stream()
//                    .sorted(Comparator.comparing(PhotoSize::getFileSize).reversed())
//                    .findFirst()
//                    .orElse(null).getHeight();
//            // Set photo caption
//            String caption = "file_id: " + f_id + "\nwidth: " + Integer.toString(f_width) + "\nheight: " + Integer.toString(f_height);
//            SendPhoto msg = new SendPhoto()
//                    .setChatId(chat_id)
//                    .setPhoto(f_id)
//                    .setCaption(caption);
//            try {
//               sendPhoto(msg); // Call method to send the photo with caption
//            } catch (TelegramApiException e) {
//                e.printStackTrace();
//            }
//        }

        if (update.hasMessage()) {
             message = update.getMessage();
            if (message.hasText()) {
                 text = message.getText();
                if (text.equals("/start")) {
                    sendMessage.setText("Assalomu alaykom birodar");
                    sendMessage.setParseMode(ParseMode.MARKDOWN);
                    sendMessage.setChatId(message.getChatId());

                    InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
                    List<List<InlineKeyboardButton>> inlineButtons = new ArrayList<>();
                    List<InlineKeyboardButton> inlineKeyboardButtonList = new ArrayList<>();

                    InlineKeyboardButton inlineKeyboardButton1 = new InlineKeyboardButton();
                    inlineKeyboardButton1.setText("Ibrohim");
                    inlineKeyboardButton1.setCallbackData("Ibrohim");

                    InlineKeyboardButton inlineKeyboardButton2 = new InlineKeyboardButton();
                    inlineKeyboardButton2.setText("Dilshod");
                    inlineKeyboardButton1.setCallbackData("Dilshod");

                    inlineKeyboardButtonList.add(inlineKeyboardButton1);
                    inlineKeyboardButtonList.add(inlineKeyboardButton2);
                    inlineButtons.add(inlineKeyboardButtonList);

                    inlineKeyboardMarkup.setKeyboard(inlineButtons);

                    sendMessage.setReplyMarkup(inlineKeyboardMarkup);

                    if (update.hasCallbackQuery()){
                        CallbackQuery callbackQuery = update.getCallbackQuery();
                        String data = callbackQuery.getData();
                        if (data.equals("Ibrohim")){
                            sendMessage.setText("Qalesan Ibrohim birodarim")
                                    .setParseMode(ParseMode.MARKDOWN)
                                    .setChatId(message.getChatId());
                        } else if (data.equals("Dilshod")){
                            sendMessage.setText("Qalesan Dilshod birodarim")
                                    .setParseMode(ParseMode.MARKDOWN)
                                    .setChatId(message.getChatId());
                        }else{
                            sendMessage.setText("Qalesz notanish kimsa )")
                                    .setParseMode(ParseMode.MARKDOWN)
                                    .setChatId(message.getChatId());
                        }
                        try {
                            execute(sendMessage);
                        } catch (TelegramApiException e) {
                            e.printStackTrace();
                        }
                    }

                    ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
                    replyKeyboardMarkup.setResizeKeyboard(true).setSelective(true);
                    sendMessage.setReplyMarkup(replyKeyboardMarkup);
                    List<KeyboardRow> keyboardRowList = new ArrayList<>();

                    KeyboardRow keyboardRow1 = new KeyboardRow();
                    KeyboardButton keyboardButton1 = new KeyboardButton();
                    keyboardButton1.setText("Tilni tanlang");
                    keyboardRow1.add(keyboardButton1);
                    //keyboardRowList.add(keyboardRow1);

                   // KeyboardRow keyboardRow2 = new KeyboardRow();
                    KeyboardButton keyboardButton2 = new KeyboardButton();
                    keyboardButton2.setText("Kontact nomerizi ulashing")
                            .setRequestContact(true);
                    keyboardRow1.add(keyboardButton2);
                    keyboardRowList.add(keyboardRow1);

                    KeyboardRow keyboardRow3 = new KeyboardRow();
                    KeyboardButton keyboardButton3 = new KeyboardButton();
                    keyboardButton3.setText("Turgan manzilizi ulashing")
                            .setRequestLocation(true);
                    keyboardRow3.add(keyboardButton3);
                    //keyboardRowList.add(keyboardRow3);

                    //KeyboardRow keyboardRow4 = new KeyboardRow();
                    KeyboardButton keyboardButton4 = new KeyboardButton();
                    KeyboardButtonPollType buttonPollType = new KeyboardButtonPollType();
                    keyboardButton4.setText("Poll shaklida so'rovnoma yaratish")
                            .setRequestPoll(buttonPollType);
                    keyboardRow3.add(keyboardButton4);
                    keyboardRowList.add(keyboardRow3);

                    replyKeyboardMarkup.setKeyboard(keyboardRowList);

                    try {
                        execute(sendMessage);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                } else if (text.equals("Tilni tanlang")) {
                    sendMessage.setText("Botda faqat uzbek tili bor )")
                            .setParseMode(ParseMode.MARKDOWN)
                            .setChatId(message.getChatId());

                    try {
                        execute(sendMessage);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }
            }else if (message.hasContact()){
                Contact contact = message.getContact();
                sendMessage.setText("Sizni telefon raqamingiz " + contact.getPhoneNumber() +
                        "\nsizni telegramde nomiz " + contact.getFirstName())
                        .setParseMode(ParseMode.MARKDOWN)
                        .setChatId(message.getChatId());

                try {
                    execute(sendMessage);
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            }else if (message.hasLocation()){
                Location location = message.getLocation();
                sendMessage.setText("GoogleMapni oching va search qismiga " + location.getLatitude() +
                        " shu id nomerni bersangiz turgan joyizi ko'rsatib beradi")
                        .setParseMode(ParseMode.MARKDOWN)
                        .setChatId(message.getChatId());

                try {
                    execute(sendMessage);
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            }else if (message.hasPoll()){
                Poll poll = message.getPoll();
                sendMessage.setText("Rahmat so'rovnomiz uchun " + poll.toString())
                        .setParseMode(ParseMode.MARKDOWN)
                        .setChatId(message.getChatId());
            }
        }
    }

}

